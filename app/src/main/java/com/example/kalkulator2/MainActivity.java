package com.example.kalkulator2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity  extends AppCompatActivity {

    private TextView txt0;
    private Button btnac,btn0,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btntambah,btnkurang,
    btnkali,btnbagi,btnhasil,btntitik;

    float hasil1,hasil2;

    boolean hasiltambah,hasilkurang,hasilkali,hasilbagi,hasiltitik;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Kalkulator");


        txt0 = (TextView) findViewById(R.id.txt_0);
        btnac = (Button) findViewById(R.id.btn_ac);
        btn0 = (Button) findViewById(R.id.nmr0);
        btn1 = (Button) findViewById(R.id.nmr1);
        btn2  = (Button) findViewById(R.id.nmr2);
        btn3 = (Button) findViewById(R.id.nmr3);
        btn4 = (Button) findViewById(R.id.nmr4);
        btn5 = (Button) findViewById(R.id.nmr5);
        btn6 = (Button) findViewById(R.id.nmr6);
        btn7 = (Button) findViewById(R.id.nmr7);
        btn8 = (Button) findViewById(R.id.nmr8);
        btn9 = (Button) findViewById(R.id.nmr9);
        btntambah = (Button) findViewById(R.id.nmr_tambah);
        btnkurang = (Button) findViewById(R.id.nmr_kurang);
        btnkali = (Button) findViewById(R.id.nmr_kali);
        btnbagi = (Button) findViewById(R.id.nmr_bagi);
        btnhasil = (Button) findViewById(R.id.nmr_hasil);
        btntitik = (Button) findViewById(R.id.nmr_titik);

        btnac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText("");
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"1");

            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"2");
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"6");
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"7");
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"8");
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"9");
            }
        });
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt0.setText(txt0.getText()+"0");
            }
        });
        btntambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txt0.getText().toString() == ""){
                    txt0.setText("");
                }
                else if (txt0.getText().toString() == "-"){
                    txt0.setText("");
                }
                else{
                    hasil1 = Float.parseFloat(txt0.getText()+"");
                    hasiltambah = true;
                    txt0.setText(null);
                }
            }
        });
        btnkurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txt0.getText().toString() == ""){
                    txt0.setText("-");
                }
                else if(txt0.getText().toString() == "-"){
                    txt0.setText("-");
                }
                else{
                    hasil1 = Float.parseFloat(txt0.getText()+"");
                    hasilkurang = true;
                    txt0.setText(null);
                }
            }
        });
        btnkali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txt0.getText().toString() == ""){
                    txt0.setText("");
                }
                else if(txt0.getText().toString() == "-"){
                    txt0.setText("");
                }
                else{
                    hasil1 = Float.parseFloat(txt0.getText()+"");
                    hasilkali = true;
                    txt0.setText(null);
                }
            }
        });
        btnbagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txt0.getText().toString() == ""){
                    txt0.setText("");
                }
                else if(txt0.getText().toString() == "-"){
                    txt0.setText("");
                }
                else{
                    hasil1 = Float.parseFloat(txt0.getText()+"");
                    hasilbagi= true;
                    txt0.setText(null);
                }
            }
        });
        btntitik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txt0.getText().toString() == ""){
                    txt0.setText("");
                }
                else if(txt0.getText().toString() == "-"){
                    txt0.setText(txt0.getText());
                }
                else{
                    txt0.setText(txt0.getText()+".");
                }
            }
        });
        btnhasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasiltambah == true){
                    hasil2 = Float.parseFloat(txt0.getText()+"");
                    txt0.setText(hasil1 + hasil2 +"");
                    hasiltambah = false;
                }
                else if (hasilkurang == true){
                    hasil2 = Float.parseFloat(txt0.getText()+"");
                    txt0.setText(hasil1 - hasil2 +"");
                    hasilkurang = false;
                }
                else if (hasilkali == true){
                    hasil2 = Float.parseFloat(txt0.getText()+"");
                    txt0.setText(hasil1 * hasil2 +"");
                    hasilkali = false;
                }
                else if (hasilbagi == true){
                    hasil2 = Float.parseFloat(txt0.getText()+"");
                    txt0.setText(hasil1 / hasil2 +"");
                    hasilbagi = false;
                }
                else if (txt0.getText().toString() == ""){
                    txt0.setText("");
                }
                else {
                    txt0.setText("");
                }
            }
        });




    }
}
